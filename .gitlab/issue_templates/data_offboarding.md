##### Remove user from

- [ ] Remove from Data team group and any projects
- [ ] Remove from Codeowners in the handbook
- [ ] Remove from Triage schedule, if applicable
- [ ] Remove from Stitch
- [ ] Remove from Snowflake
- [ ] Remove from Calendar
- [ ] Remove from Slack aliases
- [ ] Remove from Geekbot
- [ ] Remove from Google Groups- finance@
- [ ] Remove from Google Groups- analyticsapi@
- [ ] Remove from Data group in 1Password
- [ ] Unassign from issues
