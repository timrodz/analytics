Closes

List the tables added/changed below, and then run the `pgp_test` CI job.
When running the manual CI job, include the `MANIFEST_NAME` variable and input the name of the db (i.e. `gitlab_com`)

#### Tables Changed/Added

* [ ] List

#### PGP Test CI job passed?

* [ ] List
