{% docs sfdc_accounts_xf_col_ultimate_parent_account_id %}

Salesforce Account ID for Ultimate Parent Account

{% enddocs %}

{% docs sfdc_accounts_xf_col_ultimate_parent_account_name %}

Salesforce Account Name for Ultimate Parent Account

{% enddocs %}


{% docs sfdc_opportunity_stage_duration %}

This table provides the days_in_stage where the value is the greater of the aggregated days in stage or .0001 if the days in stage sums up to 0.

The calculation is done in this manner so that the average is taken out of all opportunities (including those with a value of 0).

{% enddocs %}
