{% docs sfdc_reason_for_loss_unpacked %}

This model unpackes the reason for loss field for easier querying. 

NOTE: This creates a fan out and raw counts of opportunities will be inflated.

{% enddocs %}